// Package scamalytics_go provides an unofficial access to https://scamalytics.com/ip
// IP lookup.
package scamalytics_go

// IP contains all provided infos on a specific IP.
type IP struct {
	RiskScore   int              `json:"RiskScore"`
	RiskDesc    string           `json:"RiskDesc"`
	Operator    ConnectionInfo   `json:"Operator"`
	Location    LocationInfo     `json:"Location"`
	Ports       map[int]PortInfo `json:"Ports"`
	Proxies     ProxiesInfo      `json:"Proxies"`
	DomainNames []string         `json:"DomainNames"`
}

// ConnectionInfo contains everything related to the connection in itself.
type ConnectionInfo struct {
	Hostname       string `json:"Hostname"`
	ASN            string `json:"ASN"`
	ISP            string `json:"ISP"`
	Org            string `json:"Org"`
	ConnectionType string `json:"ConnectionType"`
}

// LocationInfo contains everything related to the geographic location of the
// IP.
type LocationInfo struct {
	CountryName string `json:"CountryName"`
	CountryCode string `json:"CountryCode"`
	Region      string `json:"Region"`
	City        string `json:"City"`
	PostalCode  string `json:"PostalCode"`
	MetroCode   string `json:"MetroCode"`
	AreaCode    string `json:"AreaCode"`
	Latitude    string `json:"Latitude"`
	Longitude   string `json:"Longitude"`
}

// PortInfo contains scan result on a single port.
// All infos are optionnal.
type PortInfo struct {
	ConnectionProtocol string `json:"ConnectionProtocol"`
	Status             string `json:"Status"`
	Protocol           string `json:"Protocol"`
	Product            string `json:"Product"`
}

// ProxiesInfo is self explanaitory.
type ProxiesInfo struct {
	AnonymizingVPN    bool `json:"AnonymizingVPN"`
	TorExit           bool `json:"TorExit"`
	Server            bool `json:"Server"`
	PublicProxy       bool `json:"PublicProxy"`
	WebProxy          bool `json:"WebProxy"`
	SearchEngineRobot bool `json:"SearchEngineRobot"`
}
