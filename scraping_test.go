package scamalytics_go

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"testing"
)

func TestPageParsing(t *testing.T) {
	t.Parallel()

	happyPath, _ := ioutil.ReadFile("testdata/normal.html")
	happyIPjson, _ := ioutil.ReadFile("testdata/normal.ip")

	happyIP := IP{}
	json.Unmarshal(happyIPjson, &happyIP)

	tests := []ParsingTest{
		{
			Name:        "happy path",
			ExpectedErr: nil,
			PageBody:    string(happyPath),
			Expected:    happyIP,
		},
	}

	for _, tt := range tests {
		ip, err := parsePage(tt.PageBody)
		// TODO only once
		if tt.ExpectedErr != nil {
			if err != tt.ExpectedErr {
				t.Errorf(
					"Didn't get the expected error (\"%s\"), got \"%s\" instead",
					tt.ExpectedErr,
					err)
			}
		} else {
			if bytes.Compare(ip.JSON(), tt.Expected.JSON()) != 0 {
				t.Errorf(
					"Didn't get expected IP. Got:\n%s\nwanted:\n%s",
					ip.JSON(),
					tt.Expected.JSON(),
				)
			}
		}
	}
}

// ParsingTest contains a web page body, an expected IP and a and expected error.
// It is used to test the parsePage function
type ParsingTest struct {
	Name        string
	Expected    IP
	ExpectedErr error
	PageBody    string
}
