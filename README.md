# A simple go scrapper for scamalytics.com

Provides an IP struct containing everything scamalytics.com provides on said IP.

A simple IP lookup tool could look like that:

```go
package main

import (
	"fmt"
	scm "gitlab.com/m242/scamalytics_go"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: <program name> 1.1.1.1")
		os.Exit(0)
	}
	addr := os.Args[1]

	ip, err := scm.GetIPInfo(addr)
	if err != nil {
		fmt.Printf("Couldn't get ip info: %s", err)
		os.Exit(1)
	}
	fmt.Println(string(ip.JSON()))
}
```
