package scamalytics_go

import (
	"encoding/json"
	"reflect"
)

func (ip *IP) Equals(otherIP IP) bool {
	return reflect.DeepEqual(ip, otherIP)
}

func (ip *IP) JSON() []byte {
	s, _ := json.Marshal(ip)
	return s
}
