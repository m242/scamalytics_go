package scamalytics_go

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/anaskhan96/soup"
)

// GetIPInfo scrapes and parses scamalytics.org for a given IP.
func GetIPInfo(ip string) (IP, error) {
	req, err := http.Get(fmt.Sprintf("https://scamalytics.com/ip/%s", ip))
	if err != nil {
		log.Printf("Couldn't request IP, error : %s", err)
		return IP{}, err
	}
	bd, _ := ioutil.ReadAll(req.Body)
	return parsePage(string(bd))
}

// Parse a page. This function isn't in GetIPInfo to make testing easier.
func parsePage(body string) (IP, error) {
	rIP := IP{} // Returned IP.

	page := soup.HTMLParse(body)

	rIP.RiskDesc, rIP.RiskScore = extractRiskInfo(page)
	rIP.extractData(page)

	return rIP, nil
}

func extractRiskInfo(p soup.Root) (string, int) {
	riskDesc := "Couldn't extract"
	riskScore := -1

	divs := p.FindAll("div")
	for _, div := range divs {
		if div.Attrs()["class"] == "panel_title high_risk" {
			riskDesc = div.Text()
		}
		if div.Attrs()["class"] == "score" {
			riskScore, _ = strconv.Atoi(
				strings.Split(div.Text(), " ")[2])
		}
	}

	return riskDesc, riskScore
}

// extractData is a fucking awful
func (ip *IP) extractData(page soup.Root) {
	ip.Operator = ConnectionInfo{}
	ip.Location = LocationInfo{}
	ip.Ports = make(map[int]PortInfo)
	ip.Proxies = ProxiesInfo{}
	ip.DomainNames = make([]string, 0)

	/*
		Organization Name|Unified Layer
	*/
	tableRows := page.Find("table").FindAll("tr")
	for _, row := range tableRows {
		if len(row.Children()) == 5 {
			name := row.Find("th")
			switch name.Text() {
			case "Hostname":
				ip.Operator.Hostname = row.Find("td").Text()
				break
			case "Organization Name":
				ip.Operator.Org = row.Find("td").Text()
				break
			case "ASN":
				ip.Operator.ASN = row.Find("td").Text()
				break
			case "Connection type":
				ip.Operator.ConnectionType = row.Find("td").Text()
				break
			case "Anonymizing VPN":
				ip.Proxies.AnonymizingVPN = extractProxies(row)
				break
			case "Public Proxy":
				ip.Proxies.PublicProxy = extractProxies(row)
				break
			case "Web Proxy":
				ip.Proxies.WebProxy = extractProxies(row)
				break
			case "Search Engine Robot":
				ip.Proxies.SearchEngineRobot = extractProxies(row)
				break
			case "Tor Exit Node":
				ip.Proxies.TorExit = extractProxies(row)
				break
			case "Server":
				ip.Proxies.Server = extractProxies(row)
				break
			case "ISP Name":
				ip.Operator.ISP = row.Find("a").Text()
				break
			case "Country Name":
				ip.Location.CountryName = row.Find("td").Text()
				break
			case "Country Code":
				ip.Location.CountryName = row.Find("td").Text()
				break
			case "Region":
				ip.Location.Region = row.Find("td").Text()
				break
			case "City":
				ip.Location.City = row.Find("td").Text()
				break
			case "Postal Code":
				ip.Location.PostalCode = row.Find("td").Text()
				break
			case "Metro Code":
				ip.Location.MetroCode = row.Find("td").Text()
				break
			case "Area Code":
				ip.Location.AreaCode = row.Find("td").Text()
				break
			case "Latitude":
				ip.Location.Latitude = row.Find("td").Text()
				break
			case "Longitude":
				ip.Location.Longitude = row.Find("td").Text()
				break
			}
		}
		ip.DomainNames = extractDomains(page.Find("table"))
		ip.Ports = extractPorts(page.Find("table"))
	}
}

// extractPorts gets port info
func extractPorts(n soup.Root) map[int]PortInfo {
	r := make(map[int]PortInfo)

	rows := n.FindAll("tr")
	for _, row := range rows {
		divs := row.FindAll("div")
		if len(divs) > 0 { // We have a port
			portNum := -1
			info := PortInfo{}
			for _, div := range divs {
				switch div.Attrs()["class"] {
				case "name":
					info.ConnectionProtocol = div.Text()
				case "description":
					desc := strings.Split(div.Text(), "/")
					portNum, _ = strconv.Atoi(desc[0])
					info.Protocol = strings.Join(desc[1:],
						"/")
				case "product":
					info.Product = div.Text()
				// Port state
				case "open", "closed", "filtered":
					info.Status = div.Attrs()["class"]
				}
			}

			if portNum > 0 {
				r[portNum] = info
			}
		}
	}

	return r
}

// extractDomains returns a slice of the domains associated to an IP
func extractDomains(n soup.Root) []string {
	r := make([]string, 0)
	f := n.FindAll("td")

	for _, td := range f {
		if td.Attrs()["class"] == "colspan" {
			r = append(r, td.Text())
		}
	}

	return r
}

func extractProxies(n soup.Root) bool {
	t := n.Find("div").Text()
	if t == "Yes" {
		return true
	}
	return false
}
